/* eslint-disable */

const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const config = require('./base.config.js');

config.entry.push("webpack-hot-middleware/client");

config.devtool = "eval";

config.module.loaders = config.module.loaders.concat([
    {
        test : /\.scss$/, 
        loaders : ['style', 'css', 'sass']
    }
]);

config.plugins = config.plugins.concat([
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),

    new webpack.DefinePlugin({
        'process.env': {
            'NODE_ENV': '"development"'
        }
    }),

    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'public/index.html',
      inject: true
    })
]);

module.exports = config;