/* eslint-disable */

const webpack = require('webpack');
const path = require('path');

module.exports = {
    entry : [
        "./src/main.jsx"
    ],

    output : {
        publicPath : "/",
        path : path.resolve(__dirname, '../dist'),
        filename : "bundle.js"
    },

    resolve : {
        extensions : ['', '.scss', '.css', '.js', '.jsx'],
        root : [
            path.resolve(__dirname, '../src')
        ]
    },

    module : {
        loaders : [
            {
                test : /\.jsx?$/,
                exclude : /node_modules/, 
                loaders : ['babel']
            }
        ]
    },

    plugins : [
        
    ]
}