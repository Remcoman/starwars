/* eslint-disable */

const webpack = require('webpack');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const config = require('./base.config.js');

config.output.path = path.resolve(__dirname, '../dist');
config.output.filename = "js/[name].bundle.js";
config.output.chunkFilename = "js/[id].bundle.js";

config.devtool = "source-map";

config.module.loaders = config.module.loaders.concat([
    {
        test : /\.scss$/, 
        loader : ExtractTextPlugin.extract("style", 'css!sass')
    }
]);

config.plugins = config.plugins.concat([
    new CleanWebpackPlugin('dist', {root : path.resolve(__dirname, '..')}),
    new webpack.optimize.OccurrenceOrderPlugin(),

    new CopyWebpackPlugin([
        {from : './public'}
    ]),

    new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false
        }
    }),

    new ExtractTextPlugin("css/style.css"),

    new webpack.DefinePlugin({
        'process.env': {
            'NODE_ENV': '"production"'
        }
    }),

    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'public/index.html',
      inject: true
    })
]);

module.exports = config;