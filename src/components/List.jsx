import React, {PropTypes} from 'react';
import {Link} from 'react-router';

export default function List({className, items, itemClassName}) {
    return (
        <ol className={className}>
            {
                items.map(item => {
                    return <li key={item.url} className={itemClassName}>
                        <Link to={item.$link}>{item.name}</Link>
                    </li>
                })
            }
        </ol>
    )
}

List.propTypes = {
    items : PropTypes.arrayOf(PropTypes.object),
    itemClassName : PropTypes.string
}