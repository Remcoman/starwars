import React from 'react';

export default function ErrorMessage({className, error}) {
    return (
        <div className={className}>
            {error !== null ? error.message : ""}
        </div>
    );
}