import React, {PropTypes} from 'react';

const Row = ({prop, value, className, keyClassName, valueClassName}) => {
    return (
        <tr className={className}>
            <td className={keyClassName}>{prop}</td>
            <td className={valueClassName}>{value}</td>
        </tr>
    );
}

export default function Table({data, className, rowClassName, keyClassName, valueClassName}) {
    const keys = Object.keys(data);

    return (
        <table className={className}>
            <tbody>
            {
                keys.map( key => {
                    return <Row 
                        className={rowClassName} 
                        keyClassName={keyClassName} 
                        valueClassName={valueClassName} 
                        key={key} 
                        prop={key} 
                        value={data[key]} 
                    />;
                })
            }
            </tbody>
        </table>
    )
}

Table.propTypes = {
    data : PropTypes.object,
    rowClassName : PropTypes.string,
    keyClassName : PropTypes.string,
    valueClassName : PropTypes.string
}