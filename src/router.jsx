import React from 'react';
import {Route, IndexRoute} from 'react-router';

import App from './App';
import Categories from './modules/categories';
import Details from './modules/details';
import SubCategories from './modules/sub-categories';

export default function configureRouter() {
    return (
        <Route path="/" component={App}>
            <IndexRoute component={Categories} />
            <Route path="/:category" component={SubCategories} />
            <Route path="/:category/:item" component={Details} />
        </Route>
    );
}