//babel polyfill is needed for Promise and async
import 'babel-polyfill';

import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {browserHistory, Router} from 'react-router';

import configureStore from './store';
import configureRouter from './router';

//require main stylesheet
require('./style/style.scss');

const main = () => {
    const store  = configureStore();
    const router = configureRouter();

    const root = (
        <Provider store={store}>
            <Router history={browserHistory} children={router} />
        </Provider>
    );

    render(root, document.getElementById("app"));
}

if(!window.fetch) {
    //this will dynamically load the fetch polyfill
    require.ensure([], (require) => {
        require('isomorphic-fetch');
        main();
    });
}
else {
    main();
}