import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import bem from 'bem-classname';

import {load, unload, loadMore} from './store';
import {setHeaderTitle} from '../../store/app';

import List from '../../components/List';
import ErrorMessage from '../../components/ErrorMessage';

import './style.scss';

class SubCategories extends Component {
    constructor() {
        super();
        this.className = bem.bind(null, 'SubCategories');
    }

    componentDidMount() {
        //router parameters (see router.jsx)
        const {category} = this.props.params;

        this.props.actions.setHeaderTitle(category);
        this.props.actions.load( category );
    }

    componentWillUnmount() {
        this.props.actions.unload();
    }

    render() {
        
        let {data, loading, loadingMore, error} = this.props;

        const {className} = this;

        return (
            <div className={className({'loading' : loading, 'error' : error !== null})}>
                {error && <ErrorMessage className={className('error-message')} error={error} />}

                <List className={className('list')} itemClassName={className('item')} items={data} /> 

                <div className="layout-wrap">
                    <button className={className('more-btn') + ' ' + bem('button', {'fullWidth' : true, 'reset' : true, 'loading' : loadingMore})} onClick={this.props.actions.loadMore}>Meer</button>
                </div>
            </div>
        );
    }
}

export default connect(
    state => ({...state.subCategories.toJS(), error : state.error}),

    dispatch => ({
        dispatch,
        actions : bindActionCreators({loadMore, load, unload, setHeaderTitle}, dispatch) 
    })
)(SubCategories);
