import {Record, List} from 'immutable';
import {api} from '../../constants';
import {setCache, mergeCache, setErrorState} from '../../store/app';

export const LOAD = "subcategories/LOAD";
export const UNLOAD = "subcategories/UNLOAD";
export const LOAD_MORE = "subcategories/LOAD_MORE";
export const LOAD_MORE_DONE = "subcategories/LOAD_MORE_DONE";
export const LOAD_DONE = "subcategories/LOAD_DONE";
export const SET_RESOURCE = "subcategories/SET_RESOURCE";
export const LOAD_ABORT = "subcategories/LOAD_ABORT";

const ERROR_LOAD_FAILED = new Error("Could not sub-category from starwars api");

const initialState = Record({
    loading : false,
    loadingMore : false,
    data : List(),
    page : 1,
    category : null,
    hasNext : false
});

export const reducer = (state = initialState(), {type, payload} = {type : null, payload : null}) => {
    switch(type) {
        case SET_RESOURCE : {
            const {category} = payload;

            return state.merge({
                category : category,
            });
        }

        case UNLOAD : {
            return state.set('data', List());
        }

        case LOAD : {
            return state.merge({
                loading : true,
                page : 1
            })
        }

        case LOAD_ABORT : {
            return state.merge({
                loading : false
            });
        }

        case LOAD_MORE : {
            return state.set("loadingMore", true);
        }

        case LOAD_MORE_DONE : {
            const {results, page, hasNext} = payload;

            return state.merge({
                loadingMore : false,
                data : state.data.concat(results),
                page : page,
                hasNext
            });
        }

        case LOAD_DONE : {
            const {results, hasNext} = payload;

            return state.merge({
                loading : false,
                data : List(results),
                hasNext,
                page : 1
            });
        }
        
        default : {
            return state;
        }
    }
}

async function loadPage(category, url) {
    
    const request = await fetch(url);
    let {results, next} = await request.json();

    const cacheMap = {};
    
    results = results.map(result => {

        //there does not seem to be an id attribute so we create one from the link
        const id = result.url.slice(0,-1).split("/").pop();

        //create an internal link
        const link = `/${category}/${id}`;

        return (cacheMap[id] = {...result, $link : link, $id : id});
    });

    return {cacheMap, results, hasNext : next};
}

export function unload() {
    return {type : UNLOAD};
}

export function load(category) {
    return async function (dispatch) {
        
        dispatch({type : SET_RESOURCE, payload : {category}});
        dispatch({type : LOAD});

        let result;

        try {
            result = await loadPage(category, `${api.endPoint}${category}`);    
        }
        catch(e) {
            dispatch({type : LOAD_ABORT});
            dispatch( setErrorState(ERROR_LOAD_FAILED) );
            return;
        }

        const {cacheMap, results, hasNext} = result;

        //save the data in the cache so we don't have to request it again when going to details
        dispatch( setCache(cacheMap) );

        dispatch({type : LOAD_DONE, payload : {results, hasNext}});
    }
}

export function loadMore() {
    return async function (dispatch, getState) {
        dispatch({type : LOAD_MORE});

        const {subCategories} = getState();
        const {category, page} = subCategories;

        let nextPage = page+1, result;

        try {
            result = await loadPage(category, `${api.endPoint}${category}?page=${nextPage}`);  
        }
        catch(e) {
            dispatch({type : LOAD_ABORT});
            dispatch( setErrorState(ERROR_LOAD_FAILED) );
            return;
        }

        const {cacheMap, results, hasNext} = result;

        //same as in load only here we add to the cache instead of replacing it
        dispatch( mergeCache(cacheMap) );

        dispatch({type : LOAD_MORE_DONE, payload : {results, page : nextPage, hasNext}});
    }
}