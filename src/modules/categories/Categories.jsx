import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import bem from 'bem-classname';

import {load, unload} from './store';
import {setHeaderTitle} from '../../store/app';

import ErrorMessage from '../../components/ErrorMessage';

import './style.scss';

class Categories extends Component {
    constructor() {
        super();
        this.className = bem.bind(null, 'Categories');
    }

    componentDidMount() {
        this.props.actions.setHeaderTitle("Categories");
        this.props.actions.load();
    }

    componentWillUnmount() {
        this.props.actions.unload();
    }

    render() {
        const {className} = this;
        const {loading, error} = this.props;

        return (
            <div className={className({'loading' : loading, 'error' : error !== null})}>
                {error && <ErrorMessage className={className('error-message')} error={error} />}

                <ol className={className('list')}>
                    {
                        this.props.items.map(name => {
                            return <li className={className('item')} key={name}>
                                <Link to={"/" + name}>{name}</Link>
                            </li>;  
                        })
                    }
                </ol>
            </div>
        );
    }
}

Categories.propTypes = {
    items : PropTypes.arrayOf(PropTypes.string),
    loading : PropTypes.bool
}

export default connect(
    state => ({...state.categories.toJS(), error : state.error}),

    dispatch => ({
        dispatch,
        actions : bindActionCreators({load, setHeaderTitle, unload}, dispatch)
    })
)(Categories);