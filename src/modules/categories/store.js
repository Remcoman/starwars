import {Record, List} from 'immutable';
import {api} from '../../constants';
import {setErrorState} from '../../store/app';

export const LOAD_START = "starwars/categories/LOAD_START";
export const LOAD_DONE  = "starwars/categories/LOAD_DONE";
export const UNLOAD = "starwars/categories/UNLOAD";
export const LOAD_ABORT = "starwars/categories/LOAD_ABORT";

const ERROR_LOAD_FAILED = new Error("Could not categories from starwars api");

const initialState = Record({
    items : List(),
    loading : false
});

export function reducer(state = initialState(), {type, payload} = {type : null, payload : null}) {
    switch(type) {
        case LOAD_START : {
            return state.set('loading', true);
        }

        case UNLOAD : {
            return state.set('items', List());
        }

        case LOAD_ABORT : {
            return state.merge({
                loading : false
            });
        }

        case LOAD_DONE :
            return state.merge({
                items : List(payload),
                loading : false
            });

        default :
            return state;
    }
}

export function unload() {
    return {type : UNLOAD};
}

export function load() {
    return async function (dispatch) {
        dispatch({type : LOAD_START});

        try {
            const request = await fetch(api.endPoint);
            const result  = await request.json();
            
            dispatch({type : LOAD_DONE, payload : Object.keys(result)});
        }
        catch(e) {
            dispatch({type : LOAD_ABORT});
            dispatch( setErrorState(ERROR_LOAD_FAILED) );
        }

    }
}