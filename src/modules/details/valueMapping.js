export default {
    people : {
        mapping : {
            'name' : (value) => ({name : 'Name', value}),
            'gender' : (value) => ({name : 'Gender', value}),
            'skin_color' : (value) => ({name : 'Skin color', value}),
            'hair_color' : (value) => ({name : 'Hair color', value}),
            'height' : (value) => ({name : 'Height', value : `${value}cm`}),
            'birth_year' : (value) => ({name : 'Birthyear', value}) 
        }
    },

    planets : {
        mapping : {
            'name' : (value) => ({name : 'Name', value}),
            'rotation_period' : (value) => ({name : 'Rotation period', value}),
            'orbital_period' : (value) => ({name : 'Orbital period', value}),
            'climate' : (value) => ({name : 'Climate', value}),
            'gravity' : (value) => ({name : 'Gravity', value : value}) 
        }
    },

    films : {
        mapping : {
            'title' : (value) => ({name : 'Name', value}),
            'episode_id' : (value) => ({name : 'Episode', value}),
            'release_date' : (value) => ({name : 'Release date', value})
        }
    },

    species : {
        mapping : {
            'name' : (value) => ({name : 'Name', value}),
            'classification' : (value) => ({name : 'Classification', value}),
            'designation' : (value) => ({name : 'Designation', value})
        }
    },

    vehicles : {
        mapping : {
            'name' : (value) => ({name : 'Name', value}),
            'model' : (value) => ({name : 'Model', value}),
            'cost_in_credits' : (value) => ({name : 'Cost', value : `${value} credits`})
        }
    },

    starships : {
        mapping : {
            'name' : (value) => ({name : 'Name', value}),
            'model' : (value) => ({name : 'Model', value}),
            'cost_in_credits' : (value) => ({name : 'Cost', value : `${value} credits`}),
            'hyperdrive_rating' : (value) => ({name : 'Hyperdrive rating', value})
        }
    }
}