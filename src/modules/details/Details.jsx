import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import bem from 'bem-classname';

import {load, unload} from './store';
import {setHeaderTitle} from '../../store/app';

import Table from '../../components/Table';
import ErrorMessage from '../../components/ErrorMessage';

import valueMapping from './valueMapping';

import './style.scss';

class Details extends Component {
    constructor() {
        super();
        this.className = bem.bind(null, 'Details');
    }

    componentDidMount() {
        const {category, item} = this.props.params;

        this.props.actions.setHeaderTitle(`${category} > ${item}`);
        this.props.actions.load(category, item);
    }

    componentWillUnmount() {
        this.props.actions.unload();
    }

    mapData(data) {
        const mappedData = {};

        const {category} = this.props.params; 

        Object.keys(data).forEach(key => {
            if(valueMapping[category].mapping[key]) {
                const {name, value} = valueMapping[category].mapping[key](data[key]); 
                mappedData[name] = value;
            }
        });

        return mappedData;
    }

    render() {
        let {data, loading, error} = this.props;

        const {className} = this;

        //format the values according to the rules in valueMapping.js 
        data = this.mapData(data);

        return (
            <div className={className({'loading' : loading, 'error' : error !== null})}>
                {error && <ErrorMessage className={className('error-message')} error={error} />}

                <Table 
                    className={className('table')}
                    rowClassName={className('row')}
                    keyClassName={className('key')}
                    valueClassName={className('value')} 
                    data={data} 
                />
            </div>
        );
    }
}

export default connect(
    state => ({...state.details.toJS(), error : state.error}),

    dispatch => ({
        dispatch,
        actions : bindActionCreators({load, setHeaderTitle, unload}, dispatch)
    })
)(Details);
