import {Record, Map} from 'immutable';
import {api} from '../../constants';
import {setErrorState} from '../../store/app';

export const LOAD = "details/LOAD";
export const UNLOAD = "details/UNLOAD";
export const LOAD_DONE = "details/LOAD_DONE";
export const SET_RESOURCE = "details/SET_RESOURCE";
export const LOAD_ABORT = "details/LOAD_ABORT";

const ERROR_LOAD_FAILED = new Error("Could not details from starwars api");

const initialState = Record({
    loading : false,
    data : Map(),
    category : null,
    id : null
});

export const reducer = (state = initialState(), {type, payload} = {type : null, payload : null}) => {
    switch(type) {
        case SET_RESOURCE : {
            const {category, id} = payload;
            
            return state.merge({
                category, id
            });
        }

        case LOAD_ABORT : {
            return state.merge({
                loading : false
            });
        }

        case UNLOAD : {
            return state.set('data', Map());
        }

        case LOAD : {
            return state.set('loading', true);
        }

        case LOAD_DONE : {
            return state.merge({
                loading : false,
                data : payload
            });
        }
        
        default : {
            return state;
        }
    }
}

export function unload() {
    return {type : UNLOAD};
}

export function load(category, id) {
    return async function (dispatch, getState) {
        dispatch({type : SET_RESOURCE, payload : {category, id}});
        dispatch({type : LOAD});

        const state = getState();

        let result = state.cache[id];

        if(typeof result === "undefined") {
            try {
                const request = await fetch(`${api.endPoint}${category}/${id}`);
                result = await request.json();
            }
            catch(e) {
                dispatch({type : LOAD_ABORT});
                dispatch( setErrorState(ERROR_LOAD_FAILED) );
            }
        }
        
        dispatch({type : LOAD_DONE, payload : result});
    }
}