import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import bem from 'bem-classname';

class App extends Component {
    constructor() {
        super();
        this.className = bem.bind(null, 'app');
    }

    render() {
        const {className} = this;
        const {headerTitle} = this.props;

        return (
            <div className={className()}>
                <div className={className('header')}>{headerTitle}</div>
                {this.props.children}
            </div>
        ); 
    }
}

App.propTypes = {
    children : PropTypes.any.isRequired
}

export default connect(
    state => ({
        headerTitle : state.headerTitle
    })
)(App);