import {applyMiddleware, createStore, compose} from 'redux';
import thunk from 'redux-thunk';

//domain specific reducers
import {reducer as categories} from '../modules/categories';
import {reducer as subCategories} from '../modules/sub-categories';
import {reducer as details} from '../modules/details';

//global app reducer (for errors and stuff)
import {reducer as app} from './app';

/**
 * custom combineReducers which makes an distinction between an global reducer and domain specific reducers
 */
const combineReducers = (appReducer, moduleReducers) => {
    const moduleNames = Object.keys(moduleReducers);

    //call the given functions from right to left
    //each step returns an new state 
    const compose = (...fns) => {
        return (state, payload) => {
            for(let i=fns.length-1;i >=0;i--) {
                state = fns[i](state, payload);
            }
            return state;
        }
    }

    return compose((state, payload) => {
        const newState = Object.assign({}, state); //TODO state should be cached and be recreated when something changes
        moduleNames.forEach(name => {
            const moduleState = moduleReducers[name](newState[name], payload);
            newState[name] = moduleState;
        });
        return newState;
    }, appReducer);
}

export default function configureStore() {
    
    const rootReducer = combineReducers(app, {
        categories,
        subCategories,
        details
    });

    if(process.env.NODE_ENV === "development") { //code to enable redux devtools in development mode
        const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

        return createStore(rootReducer, 
            composeEnhancers(
                applyMiddleware(thunk)
            )
        );
    }
    else {
        return createStore(rootReducer, applyMiddleware(thunk));
    }
    
}