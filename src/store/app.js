const initialState = {
    error : null,
    headerTitle : "",
    cache : {}
}

export const SET_ERROR_STATE = "app/SET_ERROR_STATE";
export const SET_CACHE = "app/SET_CACHE";
export const SET_HEADER_TITLE = "app/SET_HEADER_TITLE";
export const MERGE_CACHE = "app/MERGE_CACHE";

export const reducer = (state = initialState, {type, payload} = {type : null, payload : null}) => {
    switch(type) {
        case SET_HEADER_TITLE : {
            return {...state, headerTitle : payload};
        }

        case MERGE_CACHE : {
            return {...state, cache : {...state.cache, payload}};
        }

        case SET_CACHE : {
            return {...state, cache : payload};
        }

        case SET_ERROR_STATE : {
            return {...state, error : payload};
        }

        default: {
            return state; 
        }
    }
}

export function setHeaderTitle(title) {
    return {type : SET_HEADER_TITLE, payload : title};
}

export function setCache(cache) {
    return {type : SET_CACHE, payload : cache};
}

export function mergeCache(cache) {
    return {type : MERGE_CACHE, payload : cache};
}

export function setErrorState(e) {
    return {type : SET_ERROR_STATE, payload : e};
}