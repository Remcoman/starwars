/* eslint-disable */

const express = require('express');
const path = require('path');

const app = express();

if(process.env.NODE_ENV == "production") {
    const redirect = (req, res) => {
        res.sendFile( path.resolve(__dirname, 'dist/index.html') );
    }

    app.use(express.static('./dist'));

    app.use('/:category', redirect);
    app.use('/:category/*', redirect);
}
else {
    const devMiddleware = require('webpack-dev-middleware');
    const hotMiddleware = require('webpack-hot-middleware');

    const devConfig = require('./config/dev.config');
    const webpack = require('webpack')(devConfig);

    var dev = devMiddleware(webpack, {});

    app.use(dev);
    app.use(hotMiddleware(webpack, {}));

    app.use('/:category', dev);
    app.use('/:category/*', dev);
}

app.listen(8080);